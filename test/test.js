/** Created by a Juan Manuel Ventura */

'use strict';

var should = require('should');
var CSV = require('../').CSV;

describe('serialization', function () {
  var objects = [
    {
      string: 'a string',
      number: 1.25
    },
    {
      string: 'another string',
      number: 2.5
    }
  ];

  var csv = CSV.serialize(objects);

  describe('options', function () {
    var csv = CSV.serialize(objects, {separator: '|'});

    it('does not add headers', function () {
      var csv = CSV.serialize(objects, {headers: false});
      csv.split('\n')[0].should.equal('a string,1.25');
    });

    it('sets custom separator', function () {
      csv.split('\n')[1].should.equal('a string|1.25');
      csv.split('\n')[2].should.equal('another string|2.5');
    });

  });

  it('adds headers', function () {
    csv.split('\n')[0].should.equal('string,number');
  });

  it('serializes into csv', function () {
    csv.split('\n')[1].should.equal('a string,1.25');
    csv.split('\n')[2].should.equal('another string,2.5');
  });

});

describe('deserialization', function () {
  var csv = 'string,number,nil,boolean\n12_a string,1.25,,true';
  var object = CSV.deserialize(csv)[0];

  describe('options', function () {

    it('accepts custom headers', function () {
      var csv = '12_a string,1.25,,true';
      var object = CSV.deserialize(csv, {headers: ['s', 'n', 'n', 'b']})[0];

      object.s.should.equal('12_a string');
    });

    it('accepts custom field separator', function () {
      var csv = 'string|number|nil|boolean\n12_a string|1.25||true';
      var object = CSV.deserialize(csv, {separator: '|'})[0];

      object.string.should.equal('12_a string');
    });

  });

  it('parses empty strings as null', function () {
    should.equal(object['nil'], null);
  });

  it('parses boolean', function () {
    object.boolean.should.be.true;
  });

  it('parses numbers', function () {
    object.number.should.equal(1.25);
  });

  it('parses strings', function () {
    object.string.should.equal('12_a string')
  });

});
