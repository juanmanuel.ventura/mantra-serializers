/** Created by Juan Manuel Ventura */

'use strict';

module.exports = {
  CSV: {
    serialize: function (arrayOfObjects, options) {
      options = options || {};

      var headers = options.headers || Object.keys(arrayOfObjects[0]);
      var separator = options.separator || ',';
      var addHeaders = options.addHeaders !== false;

      var csv = arrayOfObjects.map(function (object) {
        var line = [];

        headers.forEach(function (header) {
          line.push(object[header]);
        });

        return line.join(separator);
      });

      // add headers
      if (addHeaders) csv.unshift(headers);

      // finally convert it to CSV
      return csv.join('\n');
    },

    deserialize: function (csv, options) {
      options = options || {};

      var separator = options.separator || ',';
      var arrayOfLines = csv.split('\n');
      var arrayOfObjects = [];

      var arrayOfArrays = arrayOfLines.map(function (line) {
        return line.split(separator);
      });

      var headers = options.headers || arrayOfArrays.shift();

      arrayOfArrays.forEach(function (line) {
        var object = {};

        headers.forEach(function (header, index) {
          object[header] = parse(line[index]);
        });

        arrayOfObjects.push(object);
      });

      return arrayOfObjects;
    }
  }
};

function parse(value) {
  // missing value is converted to null
  if (value === '') return null;

  try {
    // JSON.parse won't fail when value is a boolean or a number
    return JSON.parse(value);
  } catch (e) {
    // value is a string
    return value;
  }

  // parsing dates is up to the developer and out of the scope of this library
}