A collection of serializers and deserializers

**Usage:**
```
#!javascript
var CSV = require('mantra-serializers').CSV;

var objects = [
    {
      string: 'a string',
      number: 1.25
    },
    {
      string: 'another string',
      number: 2.5
    }
  ];

var csv = CSV.serialize(objects);


```